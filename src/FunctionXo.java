import java.util.Scanner;

public class FunctionXo {

    static char[][] table;
    static char currentPlayer = 'X';
    static Scanner kb = new Scanner(System.in);

    public static void createTarang() {
        table = new char[3][3];
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                table[row][col] = ' ';
            }
        }
    }

    public static void printTarang() {
        System.out.println("-------------");
        for (int row = 0; row < 3; row++) {
            System.out.print("| ");
            for (int col = 0; col < 3; col++) {
                System.out.print(table[row][col] + " | ");
            }
            System.out.println("\n-------------");
        }
    }

    public static void printHuakor() {
        System.out.println("====================");
        System.out.println("=Welcome to XO Game=");
        System.out.println("====================");

    }

    public static void printPlayerTurn() {
        System.out.println("Player " + currentPlayer + "'s Turn");
    }

    public static boolean winOnRow() {
        for (int row = 0; row < 3; row++) {
            if (table[row][0] == currentPlayer && table[row][1] == currentPlayer && table[row][2] == currentPlayer) {
                return true;
            }
        }
        return false;
    }

    public static boolean winOnCol() {
        for (int col = 0; col < 3; col++) {
            if (table[0][col] == currentPlayer && table[1][col] == currentPlayer && table[2][col] == currentPlayer) {
                return true;
            }
        }
        return false;
    }

    public static boolean winOnCross() {
        if (table[0][0] == currentPlayer && table[1][1] == currentPlayer && table[2][2] == currentPlayer) {
            return true;
        }
        return table[0][2] == currentPlayer && table[1][1] == currentPlayer && table[2][0] == currentPlayer;
    }

    public static boolean winNaja() {
        return (winOnRow() || winOnCol() || winOnCross()) ? true : false;

    }

    public static boolean drawNaja() {
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                if (table[row][col] == ' ') {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean continueNaja() {
        String choice;
        while (true) {
            System.out.print("Continue? (y/n): ");
            choice = kb.next().toLowerCase();
            if (choice.equals("n")) {
                return false;
            } else if (choice.equals("y")) {
                break;
            } else {
                System.out.println("Please enter y or n");
            }
        }
        return true;
    }

    public static boolean outOfBound(int row, int col) {
        return row < 0 || row >= 3 || col < 0 || col >= 3;
    }

    public static boolean notEmpty(int row, int col) {
        return table[row][col] != ' ';
    }

    public static int insertRow() {
        System.out.print("Enter row (1-3): ");
        return kb.nextInt() - 1;
    }

    public static int insertCol() {
        System.out.print("Enter col (1-3): ");
        return kb.nextInt() - 1;
    }

    public static boolean hasEnd() {
        if (winNaja()) {
            System.out.println("Player " + currentPlayer + " wins!");
            return true;
        } else if (drawNaja()) {
            System.out.println("The game ends in a draw!");
            return true;
        }
        return false;
    }

    public static boolean isInvalidMove(int row, int col) {
        if (outOfBound(row, col) || notEmpty(row, col)) {
            System.out.println("Invalid move. Try again.");
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        createTarang();
        while (true) {
            printHuakor();
            printTarang();
            while (true){
                printPlayerTurn();
                int row = insertRow();
                int col = insertCol();
                if (isInvalidMove(row, col)) continue;
                table[row][col] = currentPlayer;
                printTarang();
                if (hasEnd()) break;
                currentPlayer = (currentPlayer == 'X') ? 'O' : 'X';
            }
            if (!continueNaja()) break;
        }
    }
}

